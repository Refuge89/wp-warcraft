<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package wp-warcraft
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="large-9 columns large-centered">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( '404 Error! That page can&rsquo;t be found.', 'sepia' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p class="search-form-intro"><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'sepia' ); ?></p>
					<?php get_search_form(); ?>
							<div class="row">
								<div class="large-6 columns">
									<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
									<?php if ( acn_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
										<div class="widget widget_categories">
											<h2 class="widgettitle"><?php _e( 'Most Used Categories', 'sepia' ); ?></h2>
											<ul>
											<?php
												wp_list_categories( array(
													'orderby'    => 'count',
													'order'      => 'DESC',
													'show_count' => 1,
													'title_li'   => '',
													'number'     => 10,
												) );
											?>
											</ul>
										</div><!-- .widget -->
									<?php endif; ?>
								</div>
								<div class="large-6 columns">
									<?php
										/* translators: %1$s: smiley */
										$archive_content = '<p>' . sprintf( __( 'Try looking in the monthly archives. %1$s', 'sepia' ), convert_smilies( ':)' ) ) . '</p>';
										the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
									?>
									<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>
								</div>
							</div>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
			</div>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
