<?php
/**
 * @package wp-warcraft
 */
?>

<div class="row">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="row single-portfolio-top">
			
			<div class="large-6 columns large-centered"><p class="single-portfolio-title"><?php the_title(); ?></p></div>
			
		</div>	
		<div class="row single-portfolio-hero-img">
			<div class="large-12 columns">
						<?php 
						$image = get_field('hero_portfolio_image');
						if( !empty($image) ): ?>
						<a href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> </a>
						<?php endif; ?>
						<!-- http://placehold.it/950x300" -->
			</div>
		</div>
		<div class="row single-portfolio-grid-img">	
			<ul class="large-block-grid-4" style="margin: 0 auto;">
				<li>
					<?php 
					$image = get_field('portfolio_image_1');
					if( !empty($image) ): ?>
						<a href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> </a>
					<?php endif; ?>
				</li>
				<li>
					<?php 
					$image = get_field('portfolio_image_2');
					if( !empty($image) ): ?>
						<a href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> </a>
					<?php endif; ?>
				</li>
				<li>
					<?php 
					$image = get_field('portfolio_image_3');
					if( !empty($image) ): ?>
						<a href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> </a>
					<?php endif; ?>
				</li>
				<li>
					<?php 
					$image = get_field('portfolio_image_4');
					if( !empty($image) ): ?>
						<a href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> </a>
					<?php endif; ?>	
				</li>
			</ul> 
		</div>

		<div class="large-9 columns large-centered ">
			<?php if (in_category('wp-themes')) { ?>
				<div class="single-portfolio-links">
					<div class="large-4 columns dropbox-link"><a href="<?php the_field('dropbox_link'); ?>" target="_blank">Donwload</a></div>
					<div class="large-4 columns bitbucket-link"><a href="<?php the_field('bitbucket_link'); ?>" target="_blank">BitBucket Git Repo</a></div>
					<div class="large-4 columns demo-link"><a href="<?php the_field('demo_link'); ?>" target="_blank">Theme Demo</a></div>
				</div>
			<?php } else { ?>
				<div class="single-portfolio-links">
					<div class="large-4 columns large-centered demo-link"><a href="<?php the_field('demo_link'); ?>" target="_blank">Link to live site</a></div>
				</div>
			<?php } ?> 
			<div class="single-portfolio-content">
				<?php the_content(); ?>
			</div>
		</div>

		
</article><!-- #post-## -->
</div>