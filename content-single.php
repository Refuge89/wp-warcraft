<?php
/**
 * @package wp-warcraft
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php 
		if (has_post_thumbnail( $post->ID ) ):
		    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		endif;
		$image_url = $image[0];
	?>

	<header class="entry-header hero" style="background-image: url(<?php echo $image_url; ?>); background-position: 50% 50%;">
	<div class="hero-mask">
		<div class="row">
			<div class="large-9 columns large-centered">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<div class="entry-meta">
					<?php acn_posted_on(); ?>
				</div><!-- .entry-meta -->
				<div class="header-tags">
					 <?php the_tags( '<span>', '</span><span>', '</span>' ); ?> 
				</div>
			</div>
		</div>
	</div>
	</header><!-- .entry-header -->
	
	<div class="row">
		<div class="large-9 columns large-centered">
			<div class="entry-content">
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'acn' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-footer">
				<?php acn_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		</div>
	</div>
</article><!-- #post-## -->
