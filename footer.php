<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package wp-warcraft
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row">
			<div class="large-4 columns"></div>
			<div class="large-4 columns"></div>
			<div class="large-4 columns"></div>
		</div>
		<div class="row">
			<div class="site-info">
				<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'acn' ) ); ?>"><?php printf( __( 'Powered by %s', 'acn' ), 'WordPress' ); ?></a>
				<span class="sep"> | </span>
				<?php printf( __( 'Theme: %1$s by %2$s', 'acn' ), 'WP Warcraft', '<a href="http://cristiannebunu.com" rel="designer">Cristian Nebunu</a>' ); ?> | <a href="<?php echo wp_login_url(); ?>" title="Login">Login</a>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
