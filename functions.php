<?php
/**
 * wp-warcraft functions and definitions
 *
 * @package wp-warcraft
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'acn_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function acn_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wp-warcraft, use a find and replace
	 * to change 'acn' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'acn', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'index-thumb', 1000, 400, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'acn' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'acn_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // acn_setup
add_action( 'after_setup_theme', 'acn_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function acn_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'acn' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'acn_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function acn_scripts() {
	wp_enqueue_style( 'acn-style', get_stylesheet_uri() );
	wp_enqueue_style( 'acn-opensans', 'http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700' );
	wp_enqueue_style( 'acn-ptserif', 'http://fonts.googleapis.com/css?family=PT+Serif:400italic,700italic,400,700' );
	wp_enqueue_style( 'acn-fontawesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );
	wp_enqueue_style( 'acn-foundation-styles', get_template_directory_uri() . '/css/foundation.css' );
	wp_enqueue_style( 'acn-custom-styles', get_template_directory_uri() . '/css/custom.css' );

	wp_enqueue_script( 'acn-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'acn-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'acn_scripts' );

function foundationEnqueue() {
  if (!is_admin()) {

	// deregister core jquery
	wp_deregister_script('jquery');
	// reregister jquery 2.1.0
	wp_register_script('jquery', get_template_directory_uri()."/js/vendor/jquery.js", array(),'2.1.1',false);
	// register modernizr
	wp_register_script('modernizer', get_template_directory_uri()."/js/vendor/modernizr.js",array(jquery),'2.1.0', false);
	// register foundation.min
	wp_register_script('foundation', get_template_directory_uri()."/js/foundation.min.js", array('jquery'),'2.1.0',true); 
	// register app.js - custom javascript that is theme specific
	wp_register_script('customjs', get_template_directory_uri()."/js/app.js", array('jquery'),'2.1.0',true);

	wp_enqueue_script(array('jquery','modernizer','foundation','customjs'));

  }
}
add_action( 'wp_enqueue_scripts', 'foundationEnqueue' );


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php'; 

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// Replacing [...] with read more

function new_excerpt_more( $more ) {
	return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Portfolio Custom Post Type

// Register Custom Post Type
function portfolio_items() {

	$labels = array(
		'name'                => 'Portfolio',
		'singular_name'       => 'Portfolio Item',
		'menu_name'           => 'Portfolio Item',
		'parent_item_colon'   => 'Parent Item:',
		'all_items'           => 'All Portfolio Items',
		'view_item'           => 'View Item',
		'add_new_item'        => 'Add New Portfolio Item',
		'add_new'             => 'Add New',
		'edit_item'           => 'Edit Item',
		'update_item'         => 'Update Item',
		'search_items'        => 'Search Item',
		'not_found'           => 'Not found',
		'not_found_in_trash'  => 'Not found in Trash',
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-media-document',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'portfolio', $args );

}

// Hook into the 'init' action
add_action( 'init', 'portfolio_items' );


// Adding Advanced Custom Fields to the theme

include_once('acf/acf.php');

if(function_exists("register_field_group")) {
	register_field_group(array (
		'id' => 'acf_portfolio-page',
		'title' => 'Portfolio Page',
		'fields' => array (
			array (
				'key' => 'field_550a82edbf2fa',
				'label' => 'Top Title',
				'name' => 'top_title',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_550a8305bf2fb',
				'label' => 'Top Tagline',
				'name' => 'top_tagline',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_550a8314bf2fc',
				'label' => 'Middle Title',
				'name' => 'middle_title',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_550a8315bf2fd',
				'label' => 'Middle Tagline',
				'name' => 'middle_tagline',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-portfolio.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'excerpt',
				1 => 'custom_fields',
				2 => 'discussion',
				3 => 'comments',
				4 => 'slug',
				5 => 'author',
				6 => 'format',
				7 => 'categories',
				8 => 'tags',
				9 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_single-portfolio',
		'title' => 'Single Portfolio',
		'fields' => array (
			array (
				'key' => 'field_55083936d2671',
				'label' => 'Hero Portfolio Image',
				'name' => 'hero_portfolio_image',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5508395fd2674',
				'label' => 'Portfolio Image 1',
				'name' => 'portfolio_image_1',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5508395fd2673',
				'label' => 'Portfolio Image 2',
				'name' => 'portfolio_image_2',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5508395ed2672',
				'label' => 'Portfolio Image 3',
				'name' => 'portfolio_image_3',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_55094ee027818',
				'label' => 'Portfolio Image 4',
				'name' => 'portfolio_image_4',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_55083999d2675',
				'label' => 'Dropbox Link',
				'name' => 'dropbox_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_550839bfd2677',
				'label' => 'BitBucket Link',
				'name' => 'bitbucket_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_550839bed2676',
				'label' => 'Demo Link ',
				'name' => 'demo_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'portfolio',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'excerpt',
				1 => 'custom_fields',
				2 => 'discussion',
				3 => 'comments',
				4 => 'revisions',
				5 => 'slug',
				6 => 'author',
				7 => 'format',
				8 => 'featured_image',
				9 => 'tags',
				10 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}
