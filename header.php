<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package wp-warcraft
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'acn' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="large-3 columns">
			<div class="site-branding">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<!-- <p class="site-description"><?php bloginfo( 'description' ); ?></p> -->
			</div><!-- .site-branding -->
		</div>
		<div class="large-7 columns">
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php _e( 'Primary Menu', 'acn' ); ?></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav><!-- #site-navigation -->	
		</div>
		<div class="large-2 columns">
			<span class="top-ribbon-search">
	            <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<div>
						<i class="fa fa-search"></i>
						<input type="text" placeholder="Type your query and press enter" value="<?php echo get_search_query(); ?>" name="s" id="s" />							
					</div>
				</form>
	        </span>
		</div>


			
	</header><!-- #masthead -->

	<div id="content" class="site-content">
