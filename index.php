<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package wp-warcraft
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div class="row">

			<?php if (have_posts()) : ?>
				<?php $count = 0; ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php $count++; ?>
					<?php if ($count == 1 && is_paged() == false) : ?>
						<?php get_template_part( 'content', get_post_format() ); ?>
					<?php else : ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="large-9 columns large-centered">
								<header class="entry-header">
									<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
									
									<div class="entry-meta">
										<?php acn_posted_on(); ?>
									</div><!-- .entry-meta -->
								</header><!-- .entry-header -->
							</div>

							<div class="large-9 columns large-centered">
								<?php if ( has_post_thumbnail() ) : ?>
									<a href="" title="<?php the_title_attribute(); ?>">
										<?php the_post_thumbnail('index-thumb'); ?>
									</a>
								<?php endif; ?>
							</div>

							<div class="large-9 columns large-centered">
								<div class="entry-content">
									<?php the_excerpt(); ?>
								</div><!-- .entry-content -->

								<footer class="entry-footer blog-page-entry-footer">
									<div class="row">
										<div class="large-10 columns"><?php acn_entry_footer(); ?></div>
										<div class="large-2 columns"><a class="read-more" href="<?php the_permalink() ?>">Read More</a></div>
									</div>									
								</footer><!-- .entry-footer -->
							</div>
						</article><!-- #post-## -->
					<?php endif; ?>
				<?php endwhile; ?>
					<div class="large-9 columns large-centered">
						<?php the_posts_navigation(); ?>
					</div>
			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			<?php endif; ?>


			
		</div>

		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
