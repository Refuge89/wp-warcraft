
 <?php
/*
 *  Template Name: Portfolio Page
 *
 *
 * @package wp-warcraft
 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="large-12 columns large-centered">

				<div class="top-text">
					<h1><?php the_field('top_title'); ?></h1>
					<p><?php the_field('top_tagline'); ?> </p>
				</div>

				<ul class="large-block-grid-3" style="margin: 0 auto;">
						<?php 
							// WP_Query arguments
							$args = array (
								'post_type'     => 'portfolio',
								'category_name' => 'portfolio',
							);

							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post(); ?>	
									<li>	
										<div class="portfolio-item">
											<?php 
											$image = get_field('hero_portfolio_image');
											if( !empty($image) ): ?>
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											<?php endif; ?>
											
											<div class="hover-effect-caption"><a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i><br><?php the_title(); ?></a>
											</div>
										</div>					    	
									</li>
								<?php	}
							} else {
								// no posts found
							}
							// Restore original Post Data
							wp_reset_postdata();
						?>


				</ul> 
				<div class="middle-text">
					<h1><?php the_field('middle_title'); ?> </h1>
					<p><?php the_field('middle_tagline'); ?> </p>
				</div>
				<ul class="large-block-grid-4" style="margin: 0 auto;">
					<li>
						<?php 
							// WP_Query arguments
							$args = array (
								'post_type'     => 'portfolio',
								'category_name' => 'wp-themes',
							);

							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post(); ?>
									<div class="portfolio-item">
											<?php 
											$image = get_field('hero_portfolio_image');
											if( !empty($image) ): ?>
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											<?php endif; ?>
											
											<div class="hover-effect-caption-2"><a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i><br><?php the_title(); ?></a>
											</div>
										</div>
							<?php }
							} else {
								// no posts found
							}

							// Restore original Post Data
							wp_reset_postdata();
						?>

					</li>
					
				</ul> 

				

			</div>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
