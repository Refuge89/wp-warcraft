<?php
/**
 * The template for displaying search results pages.
 *
 * @package wp-warcraft
 */

get_header(); ?>
	<div class="row">
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		
		<?php if ( have_posts() ) : ?>
			<div class="large-9 columns large-centered search-page">
			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'acn' ), '<span class="search-term">' . get_search_query() . '</span>' ); ?></h1>
				<hr>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>
			</div>
		<?php endif; ?>


		</main><!-- #main -->
	</section><!-- #primary -->
	</div>

<?php get_footer(); ?>
